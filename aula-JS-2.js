let arrayTurmaA = [
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7,
        "media": 0,
    },
    {
        "nome": "Luiz",
        "turma": "A",
        "nota1": 3,
        "nota2": 7,
        "media": 0

    }
]

let arrayTurmaB = [ {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4,
        "media": 0
    },
    {
        "nome": "Joao",
        "turma": "B",
        "nota1": 8,
        "nota2": 7,
        "media": 0
    }
]

let arrayTurmaC = [{

        "nome": "Jonathan",
        "turma": "C",
        "nota1": 9,
        "nota2": 9,
        "media": 0
    },
    {
        "nome": "Andre",
        "turma": "C",
        "nota1": 7,
        "nota2": 5,
        "media": 0
    }

]

arrayTurmaA.forEach(elem => elem.media = (elem.nota1 + elem.nota2) / 2)
arrayTurmaB.forEach(elem => elem.media = (elem.nota1 + elem.nota2) / 2)
arrayTurmaC.forEach(elem => elem.media = (elem.nota1 + elem.nota2) / 2)
//console.log(arrayTurmaA) 
//console.log(arrayTurmaB)
//console.log(arrayTurmaC)

function comparaMedia(array) {
    array.sort((a, b) => {
        if (a.media > b.media) {
            return console.log("O aluno " + a.nome + " teve a maior media da turma " + a.turma + ", com " + a.media)
        } else if (a.media < b.media) {
            return console.log("O aluno " + b.nome + " teve a maior media da turma " + b.turma + ", com " + b.media)
        } else {
            return console.log("Os dois alunos obtiveram a mesma media")
        }
    })
    
}

comparaMedia(arrayTurmaA);
comparaMedia(arrayTurmaB);
comparaMedia(arrayTurmaC);


